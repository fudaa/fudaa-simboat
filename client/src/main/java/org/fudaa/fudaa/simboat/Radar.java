/*
 * @file         Radar.java
 * @creation     2000-12-06
 * @modification $Date: 2006-09-19 15:10:27 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.simboat;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.RenderingHints;
import java.util.Vector;

import javax.swing.JPanel;

import org.fudaa.ebli.geometrie.GrMorphisme;
import org.fudaa.ebli.geometrie.GrPoint;
import org.fudaa.ebli.geometrie.GrVecteur;
/**
 * @version      $Revision: 1.8 $ $Date: 2006-09-19 15:10:27 $ by $Author: deniger $
 * @author       Nicolas Maillot
 */
public class Radar extends JPanel {
  //Nombre de mobiles
  private int nbMobiles_;
  //Pour le mode centr� sur un mobile
  //Par defaut, centr� sur le mobile n�0
  private int numMobile_= 0;
  private boolean modeCentre_;
  //Nombre de points affiches pour la trajectoire de chaque mobile
  private Vector[] trajectoires_; //En km
//  private int nombrePointsTrajectoire_;
  private int portee_= 20; //Portee en km
  private int pasCercles_= 2;
  String[] mobiles= null;
  Image imgFond_= null;
  boolean modePrevision_;
  boolean modeHistorique_;
  public Radar(
    final int _nbMobiles,
    final int _nombrePointsTrajectoire,
    final String _image,
    final boolean _modePrevision) {
    //nombrePointsTrajectoire_= _nombrePointsTrajectoire;
    nbMobiles_= _nbMobiles;
    trajectoires_= new Vector[nbMobiles_];
    for (int i= 0; i < trajectoires_.length; i++) {
      trajectoires_[i]= new Vector();
    }
    modePrevision_= _modePrevision;
    //        BuPicture pic=new BuPicture();
    imgFond_= SimboatResource.SIMBOAT.getImage(_image);
  }
  public Radar(final int _nbmobiles, final String _image) {
    this(_nbmobiles, 1, _image, true);
  }
  public void paintComponent(final Graphics g) {
    final Graphics2D g2= (Graphics2D)g;
    //Options d'affichage Java2D
    g2.setRenderingHint(
      RenderingHints.KEY_ANTIALIASING,
      RenderingHints.VALUE_ANTIALIAS_ON);
    g2.setRenderingHint(
      RenderingHints.KEY_RENDERING,
      RenderingHints.VALUE_RENDER_QUALITY);
    super.paintComponent(g2);
    final int hsize= getWidth();
    final int vsize= getHeight();
    int size= (hsize > vsize) ? vsize : hsize;
    size -= 5;
    //Rayon unitaire
    final double rayon= (size / (portee_ * 2));
    //Ajout de l'image de fond:
    //a ajouter apres, pour la transparence
    g2.drawImage(imgFond_, 0, 0, hsize, vsize, null);
    setBackground(Color.black);
    //On transforme la trajectoire pour l'affichage
    final Vector[] traj= adapteTrajectoire();
    //Affichage de chacuns des mobiles
    for (int i= 0; i < nbMobiles_; i++) {
      if ((modeCentre_ && i != numMobile_) || !modeCentre_) {
        //On prend le dernier point de la trajectoire
        final double x= ((GrPoint)traj[i].elementAt(traj[i].size() - 1)).x_;
        final double y= ((GrPoint)traj[i].elementAt(traj[i].size() - 1)).y_;
        //ne pas afficher si le mobile est hors de port�e du radar
        if (Math
          .sqrt(
            (x - hsize / 2) * (x - hsize / 2)
              + (y - vsize / 2) * (y - vsize / 2))
          <= rayon * portee_) {
          //Couleur rouge pour afficher les mobiles
          g2.setColor(Color.red);
          g2.fillOval((int)x - 2, (int)y - 2, 4, 4);
          //On rajoute l'orientation en plus si le mode prevision
          //est activ�
          if (traj[i].size() > 3 && modePrevision_) {
            final double xold= ((GrPoint)traj[i].elementAt(traj[i].size() - 3)).x_;
            final double yold= ((GrPoint)traj[i].elementAt(traj[i].size() - 3)).y_;
            g2.setColor(Color.blue);
            final double v1= (x - xold) * 3;
            final double v2= (y - yold) * 3;
            g2.drawLine((int)x, (int) (y), (int)x + (int)v1, (int)y + (int)v2);
          }
        }
      }
    }
    //Vert pour les cercles
    g2.setColor(new Color(33, 198, 77));
    //Affichage d'un cercle tous les pasCercles_ km
    for (int i= 0; i <= portee_; i += pasCercles_) {
      g2.drawOval(
        hsize / 2 - i * (int)rayon,
        vsize / 2 - i * (int)rayon,
        i * 2 * (int)rayon,
        i * 2 * (int)rayon);
    }
  }
  public void setTrajectoires(final Vector[] _trajectoires) {
    trajectoires_= _trajectoires;
  }
  public Vector[] getTrajectoires() {
    return trajectoires_;
  }
  //Adapte la trajectoire � l'ecran
  public Vector[] adapteTrajectoire() {
    final Vector[] trajtrans= new Vector[trajectoires_.length];
    for (int i= 0; i < trajectoires_.length; i++) {
      final int s= trajectoires_[i].size();
      trajtrans[i]= new Vector();
      for (int j= 0; j < s; j++) {
        trajtrans[i].add(
          new GrPoint(
            ((GrPoint)trajectoires_[i].elementAt(j)).x_,
            ((GrPoint)trajectoires_[i].elementAt(j)).y_,
            0.));
      }
    }
    final int hsize= getWidth();
    final int vsize= getHeight();
    int size= (hsize > vsize) ? vsize : hsize;
    size -= 5;
    //Vecteur utilis� pour la translation au centre de l'ecran
    GrVecteur v1= null;
    GrVecteur v2= null;
    //Mode centre en 0,0,0
    if (modeCentre_ && numMobile_ > 0 && numMobile_ < nbMobiles_) {
      final int s= trajectoires_[numMobile_].size() - 1;
      //Mode centre sur le mobile numMobile_
      v1=
        new GrVecteur(
          ((GrPoint)trajectoires_[numMobile_].elementAt(s)).x_,
          ((GrPoint)trajectoires_[numMobile_].elementAt(s)).y_,
          0.);
    }
    //centrage
    v2= new GrVecteur(getWidth() / 2, getHeight() / 2, 0.);
    for (int i= 0; i < nbMobiles_; i++) {
      for (int j= 0; j < trajectoires_[i].size(); j++) {
        //pour �tre coherent avec l'axe y des calques
         ((GrPoint)trajtrans[i].elementAt(j)).y_ *= -1;
        if (modeCentre_ && numMobile_ > 0 && numMobile_ < nbMobiles_) {
          ((GrPoint)trajtrans[i].elementAt(j)).autoApplique(
            GrMorphisme.translation(v1));
        }
        //On applique une dilatation
        ((GrPoint)trajtrans[i].elementAt(j)).autoApplique(
          GrMorphisme.dilatation(size / (portee_ * 2)));
        //Puis on translate les points, differement selon le mode
        ((GrPoint)trajtrans[i].elementAt(j)).autoApplique(
          GrMorphisme.translation(v2));
      }
    }
    return trajtrans;
  }
  public void miseAJourMobile(final int _nmobile, final GrPoint _p) {
    trajectoires_[_nmobile].add(_p);
    repaint();
  }
  public int getPasCercles() {
    return pasCercles_;
  }
  public void setPasCercles(final int _pasCercles) {
    pasCercles_= _pasCercles;
  }
  public int getnbMobiles() {
    return nbMobiles_;
  }
  public int getPortee() {
    return portee_;
  }
  public void setPortee(final int _portee) {
    portee_= _portee;
  }
  public void setModePrevision(final boolean _modePrevision) {
    modePrevision_= _modePrevision;
  }
  public boolean getModePrevision() {
    return modePrevision_;
  }
  public void setNumMobile(final int _numMobile) {
    numMobile_= _numMobile;
  }
  int getNumMobile(final int _numMobile) {
    return numMobile_;
  }
  //Centrage sur le mobile fix� par numMobile_
  public void setModeCentre(final boolean _modeCentre) {
    modeCentre_= _modeCentre;
  }
  public boolean getModeCentre() {
    return modeCentre_;
  }
}
