/*
 * @file         Simboat.java
 * @creation     2001-01-15
 * @modification $Date: 2006-09-19 15:10:27 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.simboat;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.Hashtable;

import javax.swing.ImageIcon;
import javax.swing.JFrame;

import com.memoire.bu.BuBorderLayout;
import com.memoire.bu.BuPanel;
import com.memoire.bu.BuVerticalLayout;

import org.fudaa.dodico.corba.simulation.ISimulateur;
import org.fudaa.dodico.corba.simulation.ISimulateurHelper;
import org.fudaa.dodico.corba.simulation.SMobile;
import org.fudaa.dodico.corba.simulation.SOrdreMobile;
import org.fudaa.dodico.corba.simulation.typeAffichage;

import org.fudaa.dodico.objet.CDodico;

import org.fudaa.ebli.geometrie.GrPoint;
/**
 * L'implementation du client Simboat.
 *
 * @version      $Revision: 1.8 $ $Date: 2006-09-19 15:10:27 $ by $Author: deniger $
 * @author       Nicolas Maillot
 */
public class Simboat {
  //Numero du mobile command�:
  int numMobile_;
  //Mode de visualisation:
  //O -> vision de toute la scene
  //1 -> suivi du navire
  int modeVisu_;
  //Informations sur les mobiles(x,y,z,vitesse...)
  private SMobile[] mobiles_= null;
  //Composant d'affichage des position du bateau
  SimboatAffichageImage affichage_= null;
  Radar radar_= null;
  //Contient les ordres courants
  private SOrdreMobile[] ordresMobiles_= null;
  //temps d'attente � chaque boucle
  private final int periode_= 4;
  //pour la representation et l'affichage
  //Affichage par defaut: image
  private static final typeAffichage affDefaut_= typeAffichage.IMAGE;
  private typeAffichage typeAff_= affDefaut_;
  //Ce numero correspond en fait � un repertoire dans lequel
  //est stock� la representation du mobile
  //    private static final int repDefaut_=1;
  //numero de representation
  private int[] representation_= null;
  //contient les donnees
  //cette hashtable fait correspondre � un numero de mobile l'image
  //qui lui est associ�e
  Hashtable donnees= new Hashtable();
  public void setTypeAffichage(final typeAffichage _typeAff) {
    typeAff_= _typeAff;
  }
  public typeAffichage getTypeAffichage() {
    return typeAff_;
  }
  Radar getRadar() {
    return radar_;
  }
  SimboatAffichageImage getAffichage() {
    return affichage_;
  }
  public Object getDonnees(final int _idMobile) {
    return donnees.get("" + _idMobile);
  }
  public int[] getRepresentation() {
    return representation_;
  }
  public void setRepresentation(final int[] _representation) {
    representation_= _representation;
  }
  public Simboat(final int _numMobile, final int _modeVisu) {
    numMobile_= _numMobile;
    modeVisu_= _modeVisu;
  }
  public void boucle() {
    ISimulateur nv= null;
    ImageIcon[] images= null;
    //Recherche du serveur Navmer
    nv=
      ISimulateurHelper.narrow(
        CDodico.findServerByInterface("::simulation::ISimulateur", 10000));
    if (nv == null) {
      System.err.println("connexion au serveur echouee...");
    } else {
      mobiles_= nv.mobiles();
      if (mobiles_ == null) {
        throw new RuntimeException("Probl�me de r�cup�ration de l'�tat des navires");
      }
      affichage_=
        new SimboatAffichageImage(mobiles_.length, numMobile_, modeVisu_);
      radar_= new Radar(mobiles_.length, "fond.jpg");
      //propriete du radar
      radar_.setPasCercles(2); //espace en km entre 2 cercles
      radar_.setModePrevision(true); //activation du mode prevision
      radar_.setModeCentre(false); //centre du radar sur le navire pilot�
      radar_.setNumMobile(numMobile_); //numero de mobile command�
      //Pour l'instant la representation est toujours �gal � 1
      representation_= new int[mobiles_.length];
      for (int i= 0; i < mobiles_.length; i++) {
        representation_[i]= 1;
      }
      //Composants graphique
      System.err.println("connexion etablie...");
      //Frame qui va contenir tous les elements graphiques:
      //Trajectoire, Radar, interface de commande
      final JFrame f= new JFrame("Simboat 0.8");
      f.addWindowListener(new WindowAdapter() {
        public void windowClosing(final WindowEvent e) {
          System.exit(0);
        }
      });
      f.setSize(1000, 768);
      f.setVisible(true);
      //Creation du Panel qui va tout contenir
      final BuPanel mp= new BuPanel();
      mp.setLayout(new BuBorderLayout());
      affichage_.getVueCalque().setPreferredSize(new Dimension(400, 400));
      mp.add(affichage_.getVueCalque(), BuBorderLayout.CENTER);
      //Sous-panel qui va contenir le radar et les commandes
      final BuPanel sp= new BuPanel();
      sp.setLayout(new BuVerticalLayout(2, true, true));
      //Ajout du radar
      radar_.setPreferredSize(new Dimension(125, 125));
      sp.add(radar_);
      //Ajout des commandes
      final SimboatCommande sc= new SimboatCommande();
      sc.setDestinataire(numMobile_);
      sc.setPreferredSize(new Dimension(300, 475));
      sp.add(sc);
      //Ajout du sous-panel au panel principal
      mp.add(sp, BuBorderLayout.EAST);
      f.getContentPane().add(mp);
      //Fin definition des composants graphiques
      f.repaint();
      while (true) {
        //On recupere les mobiles
        mobiles_= nv.mobiles();
        //Mise � jour de l'affichage du radar et de l'affichage
        //de la trajectoire
        images= new ImageIcon[mobiles_.length];
        final long tempsavantcalcul= System.currentTimeMillis();
        //Pour chaque mobile
        for (int i= 0; i < mobiles_.length; i++) {
          //On recupere les ordres de l'interface de commande
          ordresMobiles_= sc.getOrdres();
          //on ajoute l'image � la Hashtable si elle n'a
          //pas �t� encore charg�e
          if (donnees.get("" + i) == null) {
            //Construction de l'image � partir des donn�es demand�es
            //au serveur
            final Image img=
              (Toolkit
                .getDefaultToolkit()
                .createImage(nv.representation(typeAff_, representation_[i])));
            if (img != null) {
              donnees.put("" + i, new ImageIcon(img));
            } else {
              System.err.println("Erreur r�cuperation des images des mobiles");
            }
          }
          //On construit le tableau d'images des mobiles
          images[i]= (ImageIcon)donnees.get("" + i);
        }
        //Mise � jour de la position et du cap des navires
        //On passe �galement les images pour l'affichage
        affichage_.majAffichage(mobiles_, images);
        affichage_.getVueCalque().setBackground(new Color(224, 232, 255));
        affichage_.getVueCalque().repaint();
        //Division par 1000 car le radar prend des coordonn�es en Km
        for (int u= 0; u < mobiles_.length; u++) {
          radar_.miseAJourMobile(
            u,
            new GrPoint(mobiles_[u].x / 1000, mobiles_[u].y / 1000, 0.));
        }
        //On envoie les ordres
        nv.ordre(ordresMobiles_);
        System.err.println("***********ordres envoy�s**************");
        //on peut vider les ordres
        sc.videOrdres();
        //Delai d'attente avant la boucle suivante
        final long delay= System.currentTimeMillis() - tempsavantcalcul;
        if (delay < periode_ * 1000) {
          try {
            Thread.sleep(periode_ * 1000 - delay);
          } catch (final InterruptedException _e) {
            _e.printStackTrace();
          }
        }
      }
    }
  }
  public static void main(final String[] args) {
    Thread.currentThread().setPriority(Thread.MIN_PRIORITY);
    //On pilote le navire 0 par defaut
    int nm= 0;
    int modevisu= 0;
    System.err.println("--------------------------------");
    System.err.println("---Bienvenue dans Simboat-------");
    System.err.println("--------------------------------\n\n");
    System.err.println(
      "le programme prend deux arguments sur la ligne de commande:\n");
    System.err.println("* 1er argument: numero du navire � commander");
    System.err.println("\n\n* 2eme argument:Mode de visualisation: ");
    System.err.println("    0: vue globale de la scene");
    System.err.println("    1: vue centr�e sur le navire command�");
    System.err.println("exemple: jax simboat.Simboat 1 2");
    System.err.println("--------------------------------------\n\n");
    if (args.length == 2 || args.length == 3 || args.length == 4) {
      if (args.length == 3 || args.length == 4) {
        nm= new Double(args[2]).intValue();
        //Si on specifie un mode visu
        if (args.length == 4) {
          modevisu= new Double(args[3]).intValue();
        }
      }
      final Simboat sc= new Simboat(nm, modevisu);
      sc.boucle();
    } else {
      System.err.println("Mauvais nombre d'arguments");
    }
  }
}
