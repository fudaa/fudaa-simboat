/*
 * @file         SimboatAffichage.java
 * @creation     2000-12-06
 * @modification $Date: 2006-09-19 15:10:27 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.simboat;
import com.memoire.bu.BuPanel;

import org.fudaa.dodico.corba.simulation.SMobile;

import org.fudaa.ebli.calque.BCalqueAffichage;
import org.fudaa.ebli.calque.BGroupeCalque;
import org.fudaa.ebli.calque.BVueCalque;
/**
 * @version      $Revision: 1.6 $ $Date: 2006-09-19 15:10:27 $ by $Author: deniger $
 * @author       Nicolas Maillot
 */
public abstract class SimboatAffichage extends BuPanel {
  BVueCalque vc_= null; //Vue Calque qui affiche tous les calques
  BGroupeCalque gc_= null; //Groupe calque qui regroupe tous les calques
  BCalqueAffichage[] calques_= null;
  int numMobile_= 0; //Numero du mobile que l'affichage prend en compte
  int modeVisu_= 1; //Mode de visulalisation:
  //0: vue globale
  //1: vue centr�e sur le navire numMobile_
  public abstract void majAffichage(SMobile[] _mobiles);
  public void setNumMobile(final int _numMobile) {
    numMobile_= _numMobile;
  }
  public int getNumMobile() {
    return numMobile_;
  }
  public void setModeVisu(final int _modeVisu) {
    modeVisu_= _modeVisu;
  }
}
