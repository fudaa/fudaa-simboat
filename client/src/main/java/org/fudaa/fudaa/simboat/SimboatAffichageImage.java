/*
 * @file         SimboatAffichageImage.java
 * @creation     2000-12-06
 * @modification $Date: 2007-06-05 09:01:16 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.simboat;
import java.awt.Color;

import javax.swing.ImageIcon;

import org.fudaa.dodico.corba.simulation.SMobile;

import org.fudaa.ebli.calque.BCalqueAffichage;
import org.fudaa.ebli.calque.BCalqueImage;
import org.fudaa.ebli.calque.BGroupeCalque;
import org.fudaa.ebli.calque.BVueCalque;
import org.fudaa.ebli.geometrie.GrBoite;
import org.fudaa.ebli.geometrie.GrMorphisme;
import org.fudaa.ebli.geometrie.GrPoint;

import org.fudaa.fudaa.commun.FudaaLib;
//On consid�re dans un premier qu'il y a un calque par mobile.
//Ceci � cause des limitation des calques images
/**
 * @version      $Revision: 1.9 $ $Date: 2007-06-05 09:01:16 $ by $Author: deniger $
 * @author
 */
public class SimboatAffichageImage extends SimboatAffichage {
  public SimboatAffichageImage(final int _nbMobiles) {
    super();
    calques_= new BCalqueImage[_nbMobiles];
    gc_= new BGroupeCalque();
    for (int i= 0; i < _nbMobiles; i++) {
      calques_[i]= new BCalqueImage();
      //On place tous les calques dans le groupe calque
      gc_.add(calques_[i]);
      calques_[i].setName("Mobile n�" + i);
      calques_[i].setForeground(Color.red);
      calques_[i].setTitle(FudaaLib.getS("Mobile n�" + i));
      calques_[i].setVisible(true);
    }
    final double[][] r= new double[][] { { 10., 10., 10. }, {
        5., 5., 5. }, {
        0., 0., 0. }, {
        0., 0., 0. }
    };
    vc_= new BVueCalque(gc_);
    vc_.setBackground(Color.white);
    vc_.setRepere(r);
    /*A decommenter si l'on veut une image de fond
    //IMAGE DE FOND
    BCalqueImage f=new BCalqueImage();
    ImageIcon icon=
    new ImageIcon("/home/users/maillot/devel/fudaa/sources/fudaa_java_ecrit/org/fudaa/fudaa/simboat/mer2.jpg");
    f.setImage(icon.getImage());
    GrPoint mnw=new GrPoint(0,20000,0);
    GrPoint mne=new GrPoint(20000,20000,0);
    GrPoint msw=new GrPoint(0,0,0);
    GrPoint mse=new GrPoint(20000,0,0);
    f.setNE(mne);
    f.setNW(mnw);
    f.setSE(mse);
    f.setSW(msw);
    gc_.add(f);
    f.setTitle("Fond");
    f.setName("Fond");
    f.setVisible(true);*/
  }
  public SimboatAffichageImage(final int _nbMobiles, final int _numMobile, final int _modeVisu) {
    this(_nbMobiles);
    numMobile_= _numMobile; //Numero de mobile pris en compte
    //pour l'affichage
    modeVisu_= _modeVisu; //mode visualisation
  }
  public void majAffichage(final SMobile[] _mobiles) {}
  /*
      nouvel etat des navires
      et le images qui sont associ�es
  */
  public void majAffichage(final SMobile[] _mobiles, final ImageIcon[] _img) {
    GrBoite b= new GrBoite();
    //Pour l'instant, un calque par mobile:pas tr�s
    //optimal mais plus simple
    if (_mobiles.length != _img.length) {
      return;
    }
    //si un nouveau mobile arrive dans la simulation
    //On rajoute un calque
    if (_mobiles.length > calques_.length) {
      final BCalqueAffichage[] c= new BCalqueAffichage[_mobiles.length];
      int i;
      for (i= 0; i < calques_.length; i++) {
        c[i]= calques_[i];
      }
      for (i= calques_.length; i < _mobiles.length; i++) {
        c[i]= new BCalqueImage();
      }
      calques_= c;
    }
    for (int i= 0; i < _mobiles.length; i++) {
      //On met � jour chaque calque
      final BCalqueImage c= (BCalqueImage)calques_[i];
      c.setImage(_img[i].getImage());
      c.setRapide(false);
      final double w= _img[i].getIconWidth();
      final double h= _img[i].getIconHeight();
      //On recup�re la position du mobile
      final double x= _mobiles[i].x;
      final double y= _mobiles[i].y;
      final double z= _mobiles[i].z;
      final double cap= Math.toRadians(_mobiles[i].az);
      final GrPoint pos= new GrPoint(x, y, z);
      final GrPoint nw= new GrPoint(x - w / 2, y + h / 2, z);
      final GrPoint ne= new GrPoint(x + w / 2, y + h / 2, z);
      final GrPoint sw= new GrPoint(x - w / 2, y - h / 2, z);
      final GrPoint se= new GrPoint(x + w / 2, y - h / 2, z);
      //Rotation les points,centre de rotation: position du bateau
      GrPoint pnw, pne, psw, pse;
      pnw= nw.applique(GrMorphisme.rotation(0., 0., cap - Math.PI / 2, pos));
      pne= ne.applique(GrMorphisme.rotation(0., 0., cap - Math.PI / 2, pos));
      psw= sw.applique(GrMorphisme.rotation(0., 0., cap - Math.PI / 2, pos));
      pse= se.applique(GrMorphisme.rotation(0., 0., cap - Math.PI / 2, pos));
      //Calcul des coins en fonction de la position et du cap
      c.setNW(pnw);
      c.setNE(pne);
      c.setSW(psw);
      c.setSE(pse);
      //Si mode visu globale: 0 sur la ligne de commande
      if (i == 0) //on rafraichit une fois par boucle
        {
        b= vc_.getCalque().getDomaine();
      }
      if (modeVisu_ == 1) {
        if (i == numMobile_) {
          b.o_= new GrPoint(pos.x_ - 1000, pos.y_ - 1000, 0);
          b.e_= new GrPoint(pos.x_ + 1000, pos.y_ + 1000, 0);
        }
      }
      if ((b == null) || b.isIndefinie()) {
        return;
      }
      vc_.changeRepere(this, b);
    }
  }
  public BVueCalque getVueCalque() {
    return vc_;
  }
}
