/*
 * @file         SimboatCommande.java
 * @creation     2000-12-06
 * @modification $Date: 2006-09-19 15:10:27 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.simboat;
import java.util.Hashtable;
import java.util.Vector;

import javax.swing.JSlider;
import javax.swing.SwingConstants;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import com.memoire.bu.BuGridLayout;
import com.memoire.bu.BuPanel;
import com.memoire.bu.BuTextField;

import org.fudaa.dodico.corba.simulation.SOrdreMobile;
/**
 * @version      $Revision: 1.6 $ $Date: 2006-09-19 15:10:27 $ by $Author: deniger $
 * @author       Nicolas Maillot
 */
public class SimboatCommande extends BuPanel implements ChangeListener {
  private Vector ordres_= null;
  private int destinataire_= 0;
  private JSlider[] sliders_= null;
  private BuTextField[] tf_= null;
  //Tableau static des noms des ordres:(9 elements)
  private static final String[] nomOrdres=
    {
      "Orientation gouvernail    [ -45�,+45� ]",
      "Nombre de tours babord    [ -100%,+100% ]",
      "Nombre de tours tribord   [ -100%,+100% ]",
      "Pas de l'helice babord    [ -100%,+100% ]",
      "Pas de l'helice tribord   [ -100%,+100% ]",
      "Force propulseur avant    [ -100%,+100% ]",
      "Force propulseur arri�re  [ -100%,+100% ]",
      "Angle propulseur avant    [ -45�,+45� ]",
      "Angle propulseur arri�re  [ -45�,+45� ]" };
  private static final double[] bornesInf=
    { -45, -100, -100, -100, -100, -100, -100, -45, -45 };
  private static final double[] bornesSup=
    { 45, 100, 100, 100, 100, 100, 100, 45, 45 };
  public void setDestinataire(final int _destinataire) {
    destinataire_= _destinataire;
  }
  public int getDestinataire() {
    return destinataire_;
  }
  public SOrdreMobile[] getOrdres() {
    final SOrdreMobile[] so= new SOrdreMobile[ordres_.size()];
    for (int i= 0; i < ordres_.size(); i++) {
      so[i]= (SOrdreMobile)ordres_.elementAt(i);
    }
    return so;
  }
  public void videOrdres() {
    ordres_.removeAllElements();
  }
  public SimboatCommande() {
    //On verifie la coherence des donn�es:tous les tableaux doivent
    //avoir la m�me longueur
    if (nomOrdres.length != bornesInf.length
      || nomOrdres.length != bornesSup.length) {
      return;
    }
    ordres_= new Vector();
    //Allocation du tableau
    sliders_= new JSlider[nomOrdres.length];
    tf_= new BuTextField[nomOrdres.length];
    setLayout(new BuGridLayout(1, 8, 8));
    //on construit tous les Sliders � partir de la liste de noms
    //et des tableaux de bornes
    for (int i= 0; i < nomOrdres.length; i++) {
      tf_[i]= new BuTextField(nomOrdres[i]);
      tf_[i].setEditable(false);
      add(tf_[i]);
      sliders_[i]=
        new JSlider(
          SwingConstants.HORIZONTAL,
          (int)bornesInf[i],
          (int)bornesSup[i],
          0);
      //pas de la graduation de 5 en 5
      sliders_[i].setMajorTickSpacing(10);
      sliders_[i].setMinorTickSpacing(5);
      final Hashtable t= sliders_[i].createStandardLabels(20);
      //Affichage de la graduation
      sliders_[i].setPaintTicks(true);
      //Affichage de l'echelle
      sliders_[i].setPaintLabels(true);
      sliders_[i].setLabelTable(t);
      sliders_[i].addChangeListener(this);
      add(sliders_[i]);
    }
  }
  public void stateChanged(final ChangeEvent _e) {
    //Ajout de l'ordre dan le vecteur
    int i= 0;
    if (_e.getSource() instanceof JSlider) {
      final JSlider js= (JSlider)_e.getSource();
      while (i < sliders_.length && sliders_[i++] != js) {
        ;
      }
      if (!js.getValueIsAdjusting()) {
        double value= js.getValue();
        if (i <= 7 && i >= 2) {
          value /= 100;
        }
        System.err.println("Passage ordre: " + i + " " + value);
        ordres_.addElement(new SOrdreMobile(destinataire_, i, value));
      }
    }
  }
}
