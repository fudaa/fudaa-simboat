/*
 * @file         SimboatResource.java
 * @creation     2000-12-06
 * @modification $Date: 2005-08-16 13:29:22 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.simboat;
import com.memoire.bu.BuResource;
/**
 * Ressources pour Diapre.
 *
 * @version      $Revision: 1.5 $ $Date: 2005-08-16 13:29:22 $ by $Author: deniger $
 * @author       Guillaume Desnoix
 */
public class SimboatResource extends BuResource {
  public final static SimboatResource SIMBOAT= new SimboatResource();
}
