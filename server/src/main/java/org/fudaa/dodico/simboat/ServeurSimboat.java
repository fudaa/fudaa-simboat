/*
 * @file         ServeurSimboat.java
 * @creation     2001-02-01
 * @modification $Date: 2006-09-19 14:45:58 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.dodico.simboat;

import java.util.Date;

import org.fudaa.dodico.objet.CDodico;
import org.fudaa.dodico.objet.UsineLib;

/**
 * Classe Serveur de Simboat.
 * 
 * @version $Revision: 1.11 $ $Date: 2006-09-19 14:45:58 $ by $Author: deniger $
 * @author Nicolas Maillot
 */
public final class ServeurSimboat {

  private ServeurSimboat() {}

  public static void main(final String[] _args) {
    // essai a distance
    UsineLib.setAllLocal(false);
    final String nom = (_args.length > 0 ? _args[0] : CDodico.generateName("::simboat::ISimulateurSimboat"));
    // Cas particulier : il s'agit de creer un serveur de calcul dans une jvm donne
    // Cette M�thode n'est pas a imiter. If faut utiliser Boony pour creer des objet corba.
    CDodico.rebind(nom, UsineLib.createService(DSimulateurSimboat.class));

    System.out.println("Simboat server running... ");
    System.out.println("Name: " + nom);
    System.out.println("Date: " + new Date());
  }
}
